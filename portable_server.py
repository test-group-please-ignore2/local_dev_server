# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 07:16:49 2021

Portable flask server designed to dynamically serve pages with their paired
module.

@author: User
"""


# can only import from modules that portable_server_launcher.py has already
# imported.
from flask import Flask, redirect, url_for, request, render_template

import getpass
import sys
sys.path.append('loose_script_mods\\server_scripts')

import os
import datetime as dt
import time as T

from loose_script_mods.server_scripts.log_error_to_sharepoint import log
from loose_script_mods.server_scripts.resource_path import resource_path
from loose_script_mods.server_scripts.server_database_interface import SharepointDatabaseInterface as sdb_interface

# Global user ID, we're not using a password login as we are assuming our apps
# are being run from an internal QR environment on QR authenticated computer.
# ui set to none for legacy reasons...
"""
NOTE: ONLY variables declared AFTER the application will be considered global!
"""
application = app = Flask(__name__)

global session
session = None
user_id = getpass.getuser().upper()
cwd = os.getcwd()
ui = None

global nav_url, navigation_context
nav_url = {'current_page':'index'}
navigation_context = {}

print("Welcome " + user_id + ": Currently working in: " + cwd)
print("=DO NOT CLICK OR SELECT TEXT WITHIN THIS CONSOLE=")
print()

"""
import non standard modules that may not have been imported previously
log the error if module doens't exist then continue or exit?
"""
try:
    
    import sqlite3
    import pandas as pd
    import numpy as np

    import ast
    import webbrowser
    
    import holidays
    import random
    import openpyxl
    import pyodbc
    import networkx
    
    import importlib as importer
    import json
    
    from docxtpl import DocxTemplate
    import jinja2
    import docx
except Exception as import_error:
    log(import_error, user_id)
    pass

"""
Default environment variables
NOTE: We should sync these to user settings on our synced sharepoint drive
"""
env = "PROD"
local_settings = {}
with open("""settings\\localserver_settings.cfg""", "r") as settings_file:
    for line in settings_file.readlines():
        line = line.strip("\n")
        splitline = line.split(",")
        local_settings[splitline[0]] = splitline[1]
print("\n==SETTINGS==")
for setting in local_settings:
    print(setting + ": " + local_settings[setting])
source = ""

database_folders = {}
with open("""settings\\db_paths.cfg""", "r") as db_paths_file:
    for line in db_paths_file.readlines():
        if ("=" not in line):
            line = line.strip("\n")
            splitline = line.split(",")
            database_folders[splitline[0]] = {"source":splitline[1], "name":splitline[2], "env":splitline[3]}
print("\n==DATABASE Locations==")
for db_location in database_folders:
    print(db_location + ": " + str(database_folders[db_location]))
source = ""



"""
Use importlib to recursively import any folder that includes the string
-mod- in it.
This to allow for simple filtering of folders to what we want and cut down
on any unintentional error logging for odd folders or anomalies. 
"""
mods = {}
modpath = "loose_script_mods."
for mod_folder in os.scandir(modpath):
    if ("-mod-" in mod_folder.name):
        try:
            mods[mod_folder.name] = importer.import_module(modpath + mod_folder.name)
        except Exception as import_error:
            log(import_error, user_id)
            continue
    else:
        continue
print("\n==MODS==")
for mod in mods:
    print(mod)
mods["-mod- example"].hello_world()



# ----------------------------------------------------------------------------
# Navigation functions and url pointers here
# TODO: Create security clearances for pages and routes and set them in the 
# server url globals ductionary for referencing by the session interface.
# ----------------------------------------------------------------------------
@application.route('/')
def index():
    global nav_url
    nav_url['current_page'] = 'index'
    # login()
    return render_template(f"""{nav_url['current_page']}/{nav_url['current_page']}.html""")

@application.route('/navigate/', methods = ['POST', 'GET'])
def navigation():
    global nav_url
    values = request.values

    # expect only 1 value, that is the buttons text which we use to
    # find the related html page template and update the globals_dictionary.
    for value in values:
        if type(value) == str:
            nav_url['current_page'] = value
            ui.ui.console.appendPlainText(value)
            
            return 'http://localhost:9051/navigate_to/'

# ----------------------------------------------------------------------------
# Page templates are rendered according to the server 'current_page' global
# contained in the nav_url referenced on the creation of a server
# instance.
# ----------------------------------------------------------------------------
@application.route('/navigate_to/', methods = ['POST', 'GET'])
def return_active_page():
    global nav_url
    return render_template(f"""{nav_url['current_page']}/{nav_url['current_page']}.html""")



@application.route('/login/', methods = ['POST', 'GET'])
def login():
    return{'qualified':'youuuuuuure qualified'}

    
# @application.route('/session_add_pyrail_timetable/', methods = ['POST', 'GET'])
def session_add_pyrail_timetable(tdata, tdate):
    global session
    if tdate != None:
        timetable_date=dt.datetime.strptime(tdate, '%Y-%m-%d')
        session.session_data["timetable"] = mods["-mod- pyrail"].trains.timetable.Timetable(date=timetable_date)
    elif tdata!= None:
        session.session_data["timetable"] = mods["-mod- pyrail"].trains.timetable.Timetable(data=tdata)
    return(f"""Session has timetable for date: {timetable_date}""")

def session_export_timetable(tdata, tdate, export_name):
    session_add_pyrail_timetable(tdata, tdate)
    export = session.session_data["timetable"].functions[export_name]()
    # print(session.session_data["timetable"].functions)
    return(export)

@application.route('/export_timetable/', methods = ['POST', 'GET'])
def pyrail_export_to_tds():
    global session    
    values = request.get_json()
    data = None
    date = values["args"]["date"]
    export = values["args"]["fcall"]
    rdata = session_export_timetable(tdata=data, tdate=date, export_name=export)
    return {'data':rdata}



@application.route('/test_db_operations/', methods = ['POST', 'GET'])
def test_db_operations():
    global session
    values = request.get_json()
    db_source = database_folders[values["args"]["dbname"]]
    session.test_db_operations({"db_source":db_source, "lock_level":1, "sql":None})
    return "true"
    


def session_lock_and_return_db_conn(db_source, lock_level):
    global session
    conn = session.set_source_request_lock(db_source, lock_level)
    return conn

@application.route('/call_mod,<modname>,<funcname>/', methods = ['POST', 'GET'])
def dynamic_mod_call(modname, funcname):
    mod = mods['-mod- ' + str(modname)]
    print('\n')
    print(mod)
    print(funcname)
    
    jvals = request.get_json()
    kwargs = jvals["kwargs"]
    sqldb = jvals["sqldb"]
    priority = jvals["priority"]
    
    print(kwargs)
    print(sqldb)
    print('\n')
    
    if sqldb != None:
        dbsource = database_folders[sqldb]
        
        try:
            with session_lock_and_return_db_conn(dbsource, priority) as conn:
                kwargs['conn'] = conn
                marker = mod.mod_functions[funcname](**kwargs)
                session.relinquish_lock()
                return {'data':marker}
        except Exception as e:
            session.relinquish_lock()
            print(sys.exc_info())
            return "Failed to execute database lock: \n" + str(sys.exc_info())
    else:
        marker = mod.mod_functions[funcname](**kwargs)
        return {'data':marker}
    return "true"




# TODO: setup the local database querying
# TODO: setup the dynamic database loading (using strings from the server settings file)
"""
Run on localhost machine for now.
NOTE: Flask apps can be hosted on Azure web services which QR appears to be
towards.... maaaybe one day.
"""

# session_add_pyrail_timetable()
print("\n\n")
print("====Server hosted, details below====")
print()
if __name__ == '__main__':
    # application run options below, use 0.0.0.0 for localhost
    # application.run(host, port, debug, options)
    application.run('0.0.0.0',9051)