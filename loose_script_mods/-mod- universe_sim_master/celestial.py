# generic orbital class
import random
# from misc import ave, get_csv
from . import misc


# environment stuff
lists = "loose_script_mods/-mod- universe_sim_master/lists/"
data = "loose_script_mods/-mod- universe_sim_master/data/"


class Celestial:
    def __init__(self, name, sort,
                 type=None,
                 orbit=None,
                 orbitals=None):
        self.name = name
        self.sort = sort
        self.type = type
        self.orbit = orbit
        self.orbitals = orbitals

    def getValue(self):
        return self.name

    def create_orbitals(self, num, sort, min_dist, max_dist, unit):
        # set initial position of orbital from parent
        pos = min_dist * random.uniform(0.5, 1.5)

        # make this better
        if sort == "Planet":
            type_list = misc.get_csv(lists, "planet_types.csv")
        elif sort == "Moon":
            type_list = misc.get_csv(lists, "moon_types.csv")
        elif sort == "Satellite":
            type_list = misc.get_csv(lists, "station_types.csv")

        # generate orbitals
        for n in range(num):
            number = n + 1
            type = random.choice(type_list)

            # make this better
            if sort == "Planet":
                name = self.name + " " + misc.ave(number)
                orbit_dist = round(pos + random.uniform(min_dist,
                                                        max_dist), 3)
                orbit = str(orbit_dist) + " " + unit
            elif sort == "Satellite":
                name = sort + " " + misc.ave(number)
                orbit_dist = round(pos + random.randint(min_dist,
                                                        max_dist), 0)
                orbit = "{:,}".format(orbit_dist) + " " + unit
            elif sort == "Moon":
                name = self.name + " " + sort + " " + misc.ave(number)
                orbit_dist = round(pos + random.randint(min_dist,
                                                        max_dist), 0)
                orbit = "{:,}".format(orbit_dist) + " " + unit

            # this bit's ok
            new_orbitals = []
            new_orbital = Celestial(name, sort, type, orbit, new_orbitals)
            self.orbitals.append(new_orbital)
            pos = pos + orbit_dist
            n += 1
            print("Generated orbital")
