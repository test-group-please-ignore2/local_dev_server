import json
# import pyjsonviewer
import random
# from celestial import Celestial
from . import celestial
# from misc import get_csv
from . import misc


def mitch_is_an_excel_boi(num_years=12, max_systems=12, max_planets=12, max_moons=12, max_satellites=12):
    
    
    
    # environment stuff
    lists = "loose_script_mods/-mod- universe_sim_master/lists/"
    data = "loose_script_mods/-mod- universe_sim_master/data/"
    
    # init variables
    # num_years = int(input("Years to simulate: "))
    # max_systems = int(input("Max systems to simulate: "))
    # max_planets = int(input("Max planets per system: "))
    # max_moons = int(input("Max moons per planet: "))
    # max_satellites = int(input("Max satellites per body: "))
    
    # choose how many systems
    num_systems = random.randint(1, int(max_systems))
    
    # load lists
    sys_name_list = misc.get_csv(lists, "system_names.csv")
    sys_names = random.sample(sys_name_list, num_systems)
    
    print(str(num_systems) + " systems were generated.")
    
    universe = []
    
    # new system generation loop
    for n in range(num_systems):
        print("Generated star")
        sys_name = sys_names[n]
        planets = []
        type_list = misc.get_csv(lists, "star_types.csv")
        type = random.choice(type_list)
        sys = celestial.Celestial(sys_name, "Star", type, orbitals=planets)
        # create planets
        num_planets = random.randint(1, max_planets)
        sys.create_orbitals(num_planets,
                            "Planet", 0.1, 10, "AU")
        # create moons and satellites
        for p in sys.orbitals:
            num_satellites = random.randint(1, max_satellites)
            num_moons = random.randint(1, max_moons)
            p.create_orbitals(num_satellites,
                              "Satellite", 100, 100000, "km")
            p.create_orbitals(num_moons,
                              "Moon", 100000, 1000000, "km")
            # create moon satellites
            for q in p.orbitals:
                if q.sort == "Moon":
                    num_satellites = random.randint(1, max_satellites)
                    q.create_orbitals(num_satellites,
                                      "Satellite", 100, 100000, "km")
    
        universe.append(sys)
    
    output = json.loads(json.dumps(universe, default=lambda o: o.__dict__))
    return output
    # with open(data + "universe.json", "w") as outfile:
    #     json.dump(output, outfile)
    
    # json_file = data + "universe.json"
    # pyjsonviewer.view_data(json_file)
