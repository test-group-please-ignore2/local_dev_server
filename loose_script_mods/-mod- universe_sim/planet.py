# planet class
import random
from . import moon
from . import misc

# env = "C:/Users/Mitch/OneDrive/Code Projects/Universe Simulator/"
lists = "loose_script_mods/-mod- universe_sim/lists/"
min_orbit_diff = 100000
max_orbit_diff = 1000000

moon_type_list = misc.get_csv(lists, "moon_types.csv")


class Planet:
    def __init__(self, name, type, orbit, moons):
        self.name = name
        self.type = type
        self.orbit = orbit
        self.moons = moons

    def create_moons(self, num):
        pos = min_orbit_diff
        for n in range(num):
            name = self.name + " Moon " + misc.ave(n + 1)
            type = random.choice(moon_type_list)
            orbit_dist = pos + random.randint(min_orbit_diff, max_orbit_diff)
            orbit = "{:,}".format(orbit_dist) + " km"
            moon = moon.Moon(name, type, orbit)
            self.moons.append(moon)
            pos = pos + orbit_dist
            n = n + 1

    def return_moons(self):
        for moon in self.moons:
            print("       > " + moon.name)
            print("          - TYPE: " + moon.type)
            print("          - ORBIT: " + moon.orbit)
