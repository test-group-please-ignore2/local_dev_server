import random
from . import misc
from . import system

def generate_solar(num_years=12, max_systems=12, max_planets=12, max_moons=12):    
    # environment stuff
    # env = "C:/Users/Mitch/OneDrive/Code Projects/Universe Simulator/"
    lists = "loose_script_mods/-mod- universe_sim/lists/"
    
    # init variables REFACTORED TO FUNCTION CALL WITH DEFAULTS
    # num_years = int(input("Years to simulate: "))
    # max_systems = int(input("Max systems to simulate: "))
    # max_planets = int(input("Max planets per system: "))
    # max_moons = int(input("Max moons per planet: "))
    
    # choose how many systems
    num_systems = random.randint(1, int(max_systems))
    
    # load lists
    sys_name_list = misc.get_csv(lists, "system_names.csv")
    sys_names = random.sample(sys_name_list, num_systems)
    
    # system generation loop
    print(str(num_systems) + " systems were generated. Their names are:")
    
    for n in range(num_systems):
        num_planets = random.randint(1, int(max_planets))
        name = sys_names[n]
        planets = []
        sys = system.System(name, planets)
        sys.create_planets(num_planets)
        sys.return_system()
        
    return{'systems':num_systems}
