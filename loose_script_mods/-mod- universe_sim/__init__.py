# -*- coding: utf-8 -*-
from . import main
from . import misc
from . import moon
from . import system
from . import planet

mod_functions = {
        'simulate':main.generate_solar
    }


"""
Created on Sun May  9 15:10:49 2021


Universe sim is a module intended for use in space games requiring flavour
at the planet, system, galaxy and universe level. 



















@author: Jonas and Mitch
"""


