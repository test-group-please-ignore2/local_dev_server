# system class
import random
from . import planet
from . import misc

# env = "C:/Users/Mitch/OneDrive/Code Projects/Universe Simulator/"
lists = "loose_script_mods/-mod- universe_sim/lists/"
min_orbit_diff = 0.1
max_orbit_diff = 10
max_moons = 3

planet_type_list = misc.get_csv(lists, "planet_types.csv")


class System:
    def __init__(self, name, planets):
        self.name = name
        self.planets = planets

    def create_planets(self, num):
        # set initial distance from parent
        pos = min_orbit_diff
        for n in range(num):
            name = str(self.name) + " " + misc.ave(n + 1)
            type = random.choice(planet_type_list)
            # add a random distance to the current position
            orbit_dist = round(pos + random.uniform(min_orbit_diff,
                                                    max_orbit_diff), 3)
            orbit = str(orbit_dist) + " AU"
            moons = []
            planet =  planet.Planet(name, type, orbit, moons)
            num_moons = random.randint(0, max_moons)
            planet.create_moons(num_moons)
            self.planets.append(planet)
            pos = pos + orbit_dist
            n = n + 1

    def return_system(self):
        print(self.name)
        for planet in self.planets:
            print(" > " + planet.name)
            print("    - ORBIT: " + planet.orbit)
            print("    - TYPE: " + planet.type)
            print("    - MOONS:")
            planet.return_moons()
