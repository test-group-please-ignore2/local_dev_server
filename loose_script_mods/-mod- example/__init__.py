from .example import *

__doc__ = """
Blank mod folder to illustrate proper file structure and naming convention.

RULES:
    -Mods will only be loaded automatically if they have the -mod- tag 
    somewhere in their name (at the beginning is the accepted standard).
    
    -All mods must have an __init__.py to describe what scripts to load 
    and their naming.
"""
