# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 15:54:51 2020

@author: r907349
"""

import pandas as pd
import pyodbc
# import time
# import datetime as dt
# import os
# try:
#     from . import db_operations as dbo
# except ImportError:
#     import db_operations as dbo

def user_details_query():
    query = """
    SELECT
    usr_ID,
    usr_DisplayName as DisplayName,
    usr_Privilige as AccessLevel,
    usr_Team as Team,
    usr_IsActive as IsActive
    FROM RMC_Users
    --WHERE usr_IsActive=1
    ORDER BY usr_DisplayName
    """
    return query

#@dbo.db_lock_fn()#vacuum_type='force',user='IMPORT')
def get_pb_tn_link(bid_list):
    sql_bid_list = '('+','.join([f'\'{x}\'' for x in bid_list])+')'
    link_query = f"""
    SELECT * FROM PB_TN_Link
    WHERE pb_Number in {sql_bid_list}
    """
    return link_query

def add_pb_tn_link(source,bid_id,tn_id):
    add_query = f"""
    INSERT INTO PB_TN_Link (pb_Number,tn_Number)
    VALUES (\'{bid_id}\',\'{tn_id}\');
    """
    return add_query

def del_pb_tn_link(source,bid_id,tn_id):
    del_query = f"""
    DELETE FROM PB_TN_Link
    WHERE pb_Number=\'{bid_id}\'
    AND tn_Number= \'{tn_id}\';
    """
    return del_query

#just get 'em all
def get_all_local_bids(source):

    query = """SELECT * FROM TR_Bids ORDER BY pbt_StartDate"""

    # with dbo.database_conn(source,isolation_level='DEFERRED') as conn:
    #     local_bids = (
    #         pd
    #         .read_sql(query,conn,index_col='pb_Number')
    #         .drop_duplicates(keep='first')
    #         )
    return query

# def archive_bids(source,archived_bids,date_grouping):
#     grouping_str = date_grouping.strftime('%Y-%m')
#     with dbo.archive_conn(source,grouping_str) as conn:
#         (
#             archived_bids
#             .drop_duplicates(keep='first')
#             .to_sql('TR_Bids', con=conn, if_exists='append')
#             )
#     return

#overwrite bids in local
# def set_local_bids(source,new_bids):
#     with dbo.database_conn(source, isolation_level='DEFERRED') as conn:

#         (
#             new_bids
#             .drop_duplicates(keep='first')
#             .to_sql('TR_Bids', con=conn, if_exists='replace')
#             )
#     return

def get_cancellations():
    query = """
    SELECT [pb_Number]
    FROM [Vizirail_QRail_SP_Reporting].[dbo].[VR_ADVPossessionBid]
    where pb_gc_id_status in (580,581)
    """
    vizi_conn = """
    DRIVER={SQL Server};
    SERVER=SQLPRDVIZRPT\S01,56420,
    DATABASE=Vizirail_Qrail_SP_Reporting;
    Trusted_Connection=yes;
    """
    with pyodbc.connect(vizi_conn) as conn:
        cur = conn.cursor()
        cur.execute(query)
        result = [bid[0] for bid in cur.fetchall()]

    return result
def get_vizirail_bids(inclusion_list=[],exclusion_list=[]):
    query = f"""
DECLARE @StartDate DATETIME;
DECLARE @StartDateMod DATETIME;
DECLARE @LastDate DATETIME;

SET @StartDate		= GETDATE();
SET @StartDateMod   = DATEADD(DAY, -14, @StartDate);

WITH   BidsWithSCAS AS
(
SELECT DISTINCT
       [pbw_pb_id]
      ,VR_REFCode.[gc_Name] AS WorkType
  FROM [Vizirail_QRail_SP_Reporting].[dbo].[VR_ADVPossessionBidWorkItem]
  LEFT JOIN [Vizirail_QRail_SP_Reporting].[dbo].[VR_REFCode]
  ON VR_ADVPossessionBidWorkItem.pbw_gc_id_WorkItem = VR_REFCode.gc_id
),
   FINAL AS
(
SELECT
       pbt_id
      ,[VR_ADVPossessionBid].[CreationUser] as Creator
      ,VR_ADVPossessionBid.[pb_Number]
      ,[su_Code]
      ,VR_ADVPossessionBid.[pb_gc_id_Status]
      ,a.[pbt_StartDate]
      ,a.[pbt_EndDate]
      ,BidsWithSCAS.WorkType
      ,VR_ADVPossessionBid.[pb_OverheadLineEquipmentCommentsText]
      ,VR_ADVPossessionBid.[pb_GeneralCommentsText]
      ,VR_ADVPossessionBid.[pb_MilestoneCommentsText]
      ,VR_ADVPossessionBid.[pb_TrafficCommentsText]
      ,ROW_NUMBER() OVER(PARTITION BY pb_Number ORDER BY VR_ADVPossessionBid.LastUpdateDatetime DESC) AS RowNumber
  FROM [Vizirail_QRail_SP_Reporting].[dbo].[VR_ADVPossessionBid]

  LEFT JOIN [Vizirail_QRail_SP_Reporting].[dbo].[VR_SECSecurityObject]
  ON [Vizirail_QRail_SP_Reporting].[dbo].[VR_ADVPossessionBid].[pb_su_id_PlanningOfficer] = [Vizirail_QRail_SP_Reporting].[dbo].[VR_SECSecurityObject].[su_id]
  LEFT JOIN [Vizirail_QRail_SP_Reporting].[dbo].[VR_ADVPossessionBidTime] a
  ON VR_ADVPossessionBid.pb_id = a.pbt_pb_id

  LEFT JOIN BidsWithSCAS
  ON VR_ADVPossessionBid.pb_id = BidsWithSCAS.pbw_pb_id

 WHERE (pb_pb_id_Original = pb_id
   AND pb_ct_id = '6'
   AND ((pbt_StartDate > @StartDateMod) )
   )
)
SELECT *
FROM FINAL

WHERE RowNumber = 1
AND pb_gc_id_status not in (580,581)

ORDER BY pbt_StartDate, pb_Number
"""
    with pyodbc.connect('DRIVER={SQL Server};SERVER=SQLPRDVIZRPT\S01,56420,DATABASE=Vizirail_Qrail_SP_Reporting;Trusted_Connection=yes;') as conn:
        query_data = pd.read_sql(
            query,conn,index_col='pb_Number'
            ).drop_duplicates(keep='first')

    return query_data

def get_redbook_notice(source,notice_id):
    query = f"""
    WITH Check_Agg AS (
    SELECT
        tn_Number,
        '||' || GROUP_CONCAT(
            'Checker:' || Checker || '/Time:' || Time,
            ';'
        ) AS Check_Details
    FROM Checks
    GROUP BY tn_Number
    )
    SELECT
        TR_TrainNotices.tn_Number,
        TR_TrainNotices.tn_Title,
        Check_Details
    from TR_TrainNotices
    LEFT JOIN Check_Agg
    on TR_TrainNotices.tn_Number=Check_Agg.tn_Number
    WHERE TR_TrainNotices.tn_Number = '{notice_id}'
    """
    return query

def get_vizirail_notice(notice_id):
    query = f"""
        SELECT DISTINCT
        [tn_Number],
        [tn_Title],
        [LastUpdateUser] as tn_LastUpdateUser,
        [tn_IssueDate],
        [tn_EffectiveFrom],
        [tn_EffectiveTo]
        FROM [Vizirail_QRail_SP_Reporting].[dbo].[VR_ADVTrainNotice]
        WHERE tn_Number = '{notice_id}'
        """
    return query

def get_notice_body(notice_id):
    query=f"""
        SELECT tn_DetailsText
        FROM [Vizirail_QRail_SP_Reporting].[dbo].[VR_ADVTrainNotice]
        WHERE tn_Number = \'{notice_id}\'
        """
    return query

def workload_report_query(
    assigned_planner='ALL',
    assigned_team='ALL',
    date_from=None,
    date_to=None,
    ):

    completion_query = f"""
    SELECT 1 FROM PB_TN_Link
    WHERE TR_Bids.pb_Number = pb_Number
    """

    query_conds = [f'NOT EXISTS({completion_query})']

    dt_format = '%Y-%m-%d %H:%M:%S'

    if assigned_planner is None:
        query_conds.append(f"su_Code is NULL")
    elif assigned_planner.upper() != 'ALL':
        query_conds.append(f"su_Code = '{assigned_planner}'")

    if assigned_team.upper() in ['NONE','UNASSIGNED']:
        query_conds.append(f"TR_Bids.usr_Team is NULL")
    elif assigned_team.upper() not in ['ALL','ANY']:
        query_conds.append(f"TR_Bids.usr_Team = '{assigned_team}'")
    if date_from:
        start_date = date_from.strftime(dt_format)
        query_conds.append(f'pbt_StartDate >= \'{start_date}\'')
    if date_to:
        end_date = date_to.strftime(dt_format)
        query_conds.append(f'pbt_StartDate <= \'{end_date}\'')

    if query_conds:
        query_filter = f"""where {' and '.join(query_conds)}"""
    else:
        query_filter = ''

    query = f"""
    SELECT
        TR_Bids.pb_Number,
        pbt_StartDate AS Date,
        su_Code AS Planner_ID,
        TR_Bids.usr_Team AS Team,
        usr_DisplayName AS Planner
    FROM TR_Bids
    LEFT JOIN RMC_Users
    on TR_Bids.su_Code = RMC_Users.usr_ID
    {query_filter}
    ORDER BY pbt_StartDate"""
    return query

def checks_report_query(
    planner='ALL',
    team='ALL',
    date_from=None,
    date_to=None,
    ):
    query_conds = []

    dt_format = '%Y-%m-%d %H:%M:%S'

    if planner is None:
        pass
    elif planner.upper() != 'ALL':
        query_conds.append(f"Checker = '{planner}'")

    if team.upper() in ['NONE','UNASSIGNED']:
        pass
    elif team.upper() not in ['ALL','ANY']:
        query_conds.append(f"usr_Team = '{team}'")

    if date_from:
        start_date = date_from.strftime(dt_format)
        query_conds.append(f'tn_IssueDate >= \'{start_date}\'')

    if date_to:
        end_date = date_to.strftime(dt_format)
        query_conds.append(f'tn_IssueDate <= \'{end_date}\'')

    if query_conds:
        query_filter = f"""where {' and '.join(query_conds)}"""
    else:
        query_filter = ''

    query = f"""
    SELECT
        Checks.tn_Number,
        Checker AS Checker_ID,
        Time,
        tn_IssueDate AS Date,
        usr_Team AS Team,
        usr_DisplayName AS Checker
    FROM Checks
    LEFT JOIN RMC_Users
    on Checks.Checker = RMC_Users.usr_ID
    LEFT JOIN TR_TrainNotices
    on Checks.tn_Number = TR_TrainNotices.tn_Number
    {query_filter}
    ORDER BY tn_IssueDate"""
    return query

def daily_report_query():
    team_report = f"""
        SELECT pb_Number, pbt_StartDate,
        pbt_EndDate,
        pb_GeneralCommentsText,
        usr_Team
        FROM TR_Bids
        WHERE su_Code IS NULL AND
        pbt_StartDate < (DATE('now', '+71 day'))
        ORDER by pbt_StartDate
        """
    return team_report

def bid_data_query(
        bid_list=[],
        bid_status='ALL',
        assigned_planner='ALL',
        assigned_team='ALL',
        date_from=None,
        date_to=None,
        ):
    query_conds = []

    dt_format = '%Y-%m-%d %H:%M:%S'

    completion_query = f"""
    SELECT 1 FROM PB_TN_Link
    WHERE TR_Bids.pb_Number = pb_Number
    """

    if bid_list:
        bid_query = (
            f"SUBSTR(TR_Bids.pb_Number,1,{len(bid_list[0])}) = '{bid_list[0]}'" +
            '\n'.join([f"OR SUBSTR(TR_Bids.pb_Number,1,{len(i)}) = '{i}'" for i in bid_list[1:]])
            )
        query_conds.append(f"({bid_query})")
    if bid_status.upper() == 'COMPLETE':
        query_conds.append(f'EXISTS({completion_query})')
    elif bid_status.upper() == 'INCOMPLETE':
        query_conds.append(f'NOT EXISTS({completion_query})')

    if assigned_planner is None:
        query_conds.append(f"su_Code is NULL")
    elif assigned_planner.upper() != 'ALL':
        query_conds.append(f"su_Code = '{assigned_planner}'")

    if assigned_team.upper() in ['NONE','UNASSIGNED']:
        query_conds.append(f"usr_Team is NULL")
    elif assigned_team.upper() not in ['ALL','ANY']:
        query_conds.append(f"usr_Team = '{assigned_team}'")
    if date_from:
        start_date = date_from.strftime(dt_format)
        query_conds.append(f'pbt_StartDate >= \'{start_date}\'')
    if date_to:
        end_date = date_to.strftime(dt_format)
        query_conds.append(f'pbt_StartDate <= \'{end_date}\'')

    if query_conds:
        query_filter = f"""where {' and '.join(query_conds)}"""
    else:
        query_filter = ''

    query = f"""
    WITH TNs AS (
    SELECT
        pb_Number,
        '|' || GROUP_CONCAT(
            tn_Number,';'
            )
        as Train_Notices
        FROM PB_TN_Link
        GROUP BY pb_Number
        )
    SELECT
        TR_Bids.pb_Number,
        pbt_StartDate,
        pbt_EndDate,
        WorkType,
        usr_Comments,
        pb_GeneralCommentsText,
        pb_MilestoneCommentsText,
        pb_OverheadLineEquipmentCommentsText,
        su_Code,
        usr_Team,
        pb_TrafficCommentsText,
        COALESCE(Train_Notices,'|') as Train_Notices,
        usr_Is_CSA_Bid,
        usr_Is_TSA
    FROM TR_Bids
    LEFT JOIN TNs
    on TNs.pb_Number=TR_Bids.pb_Number
    {query_filter}
    ORDER BY pbt_StartDate"""
    return query

def tn_data_query(tn_list=[]):
    sql_tn_list = '('+','.join([f'\'{x}\'' for x in tn_list])+')'
    query = f"""
    WITH Check_Agg AS (
    SELECT
        tn_Number,
        '||' || GROUP_CONCAT(
            'Checker:' || Checker || '/Time:' || Time || '/Check_Num:' || Check_Num,
            ';'
        ) AS Check_Details
    FROM Checks
    GROUP BY tn_Number
    )
    SELECT
        TR_TrainNotices.tn_Number,
        TR_TrainNotices.tn_Title,
        COALESCE(Check_Details,'||') as Check_Details
    from TR_TrainNotices
    LEFT JOIN Check_Agg
    on TR_TrainNotices.tn_Number=Check_Agg.tn_Number
    where TR_TrainNotices.tn_Number in {sql_tn_list}
    """
    return query

def generate_change_queries(change_dict,table,id_col):
    query_dict = {
        id_val : (
        f'UPDATE {table} SET ' +
        ',\n'.join([
            f'{changed_col}=:{changed_col}'
            for changed_col in change_dict[id_val]
            ]) +
        '\n' +
        f'WHERE {id_col}="{id_val}"'
        )
        for id_val in change_dict
        }
    return query_dict

def check_delete_query(tn_list):
    list_str=','.join([f'\'{tn}\'' for tn in tn_list])
    del_query=f"""
    DELETE FROM CHECKS
    WHERE tn_Number in ({list_str})
    """
    return del_query

def checks_update_query():
    check_cols=['tn_Number','Checker','Time','Check_Num']
    cols_str=','.join(check_cols)
    insert_str=','.join([f':{col}' for col in check_cols])
    insert_query = f"""
    INSERT INTO CHECKS ({cols_str})
    VALUES ({insert_str})
    """
    return insert_query

def links_delete_query(bid_list):
    list_str=','.join([f'\'{bid}\'' for bid in bid_list])
    del_query=f"""
    DELETE FROM PB_TN_LINK
    WHERE pb_Number in ({list_str})
    """
    return del_query

def links_update_query():
    link_cols=['pb_Number','tn_Number']
    cols_str=','.join(link_cols)
    insert_str=','.join([f':{col}' for col in link_cols])
    insert_query = f'''
    INSERT INTO PB_TN_Link ({cols_str}) VALUES ({insert_str})
    '''
    return insert_query

def tn_insert_query():
    tn_columns=[
    'tn_Number',
    'tn_Title',
    'tn_LastUpdateUser',
    'tn_IssueDate',
    'tn_EffectiveFrom',
    'tn_EffectiveTo',
    ]
    cols_str=','.join(tn_columns)
    insert_str=','.join([f':{col}' for col in tn_columns])
    insert_query = f'''
    INSERT INTO TR_TrainNotices ({cols_str}) VALUES ({insert_str})
    '''
    return insert_query

