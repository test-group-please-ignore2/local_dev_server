"""
Created on Wed Apr  1 19:33:04 2020

@author: r908394
"""

import sqlite3
import getpass
import datetime as dt
import time



# def locking_request(locking_level=0):
#     def inner_decorator(fn):
#         def permission_restricted_function(*args,**kwargs):
#             print(f"""Locking at level: {locking_level}""")
#             with database_connection_lock(session.database_source) as lock_conn: 
            
#             current_lock = test_db_lock()
#             user_level=args[0].permission
            
            
#             if user_level > locking_level:
#                 raise PermissionError('You don\'t have access to that functionality')
#             else:
#                 return fn(*args,**kwargs)
#         return permission_restricted_function
#     return inner_decorator

 

# TODO: investigate @wraps functionality for locking the database
def test_dbo(conn, sql_query):
    return

def update_dbo(session, sql_query):
    return

def insert_dbo(session, sql_query):
    return

def delete_dbo(session, sql_query):
    return

def select_dbo(session, sql_query):
    return

def drop_dbo(session, sql_query):
    return

def check_vacuum_integrity(dbo_cursor):
    dbo_cursor.execute("""
            SELECT Vacuum_Counter
            FROM lockstate
            """)
    return(dbo_cursor.fetchone()[0])

def test_db_lock(dbo_cursor):
    open_db_range = ["0", 0, None, "null", "none", "None"]
    medium_lock_range = [1]
    high_lock_range = [2, "IMPORT", "Import", "import"]
    try:
        query=f"""
        SELECT Participant, Lock_Time, Lock_Type FROM lockstate
        """
        dbo_cursor.execute(query)
        results = dbo_cursor.fetchall()[0]
        print(results)
        if results[2] not in open_db_range:
            print("locked")
            return (True, results)
        elif results[2] in open_db_range:
            return (False, results)
        
    except Exception as e:
        print(e)
        return (True, e)
        pass



def database_connection(db_source):
    try:
        source = db_source["source"]
        name = db_source["name"]
        env = db_source["env"]
        db_connection = sqlite3.connect(f"""{source}\\{env}\\{name}.db""")
        return db_connection
    except Exception as e:
        print(e)
        return
    

def database_archive_connection(db_source):
    try:
        source = db_source["source"]
        name = db_source["name"]
        env = db_source["env"]
        db_connection = sqlite3.connect(f"""{source}\\{env}\\{name}_archive.db""")
        return db_connection
    except Exception as e:
        print(e)
        return

def database_connection_lock(db_source):
    try:
        source = db_source["source"]
        name = db_source["name"]
        env = db_source["env"]
        print(f"""{source}\\{env}\\sqlite_lock.db""")
        db_connection = sqlite3.connect(f"""{source}\\{env}\\sqlite_lock.db""")
        return db_connection
    except Exception as e:
        print(e)
        return


def relinquish_db_lock(session):
    print("relinquishing lock")
    lock_time = dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    try:
        with database_connection_lock(session.database_source) as lock_conn:
            lock_cur = lock_conn.cursor()
            vacuum_count = check_vacuum_integrity(lock_cur)
            query=f"""
            UPDATE lockstate
            SET Participant = null,
            Lock_Time='{lock_time}',
            Lock_Type=0;
            """
            lock_cur.execute(query)
            lock_conn.commit()
            return "success"
    except Exception as e:
        print(e)
    return "success"

def request_db_lock(session, lock_level):
    with database_connection_lock(session.database_source) as lock_conn:
        try:
            lock_time = dt.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
            
            lock_cur = lock_conn.cursor()
            vacuum_count = check_vacuum_integrity(lock_cur)
            locked = test_db_lock(lock_cur)
            if not locked[0]:
                if int(lock_level) > 0:
                    print(f"""request_db_lock: {locked}""")
                    query=f"""
                    UPDATE lockstate
                    SET Participant = '{session.user['fullname']}',
                    Lock_Time='{lock_time}',
                    Lock_Type='{lock_level}';
                    """
                    lock_cur.execute(query)
                    lock_conn.commit()
                return database_connection(session.database_source)
        except Exception as e:
            # lock_conn.close()
            print("!!!Failed to lock db!!! \n")
            print(e)
            return False
            









def loaded():
    print("server_db_operations.py loaded")
    return 0