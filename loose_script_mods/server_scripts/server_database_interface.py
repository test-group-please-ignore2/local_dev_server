# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 10:37:19 2021

Flask Database (SQlite) lock and access class responsibe for determining which
database, access privilige and database environment variables.

Does not manage main data writing operations, may write to lock files
for managing concurrent access to databases. 

@author: User
"""


import sys
import os
import sqlite3
import getpass

from . import server_db_operations as dbo
from . import server_sql as queries
dbo.loaded()


def restrict_function_permission(permission_level=0):
    def inner_decorator(fn):
        def permission_restricted_function(*args,**kwargs):
            print("testing security clearance")
            user_level=args[0].permission
            if user_level > permission_level:
                raise PermissionError('You don\'t have access to that functionality')
            else:
                return fn(*args,**kwargs)
        return permission_restricted_function
    return inner_decorator

def localserver_login(user_db_location):    
    with dbo.database_connection(user_db_location) as conn:
        cur = conn.cursor()
        
        cur.execute(f"""SELECT * FROM RMC_Users""")
        users = cur.fetchall()
        user_dict = {}
        for user in users:
            user_dict[user[0]] = {"fname":user[1], 
                                  "lname":user[2], 
                                  "fullname":user[3], 
                                  "email":user[4], 
                                  "level":user[5], 
                                  "privilige":user[6], 
                                  "adjective":user[7], 
                                  "team":user[8], 
                                  "isactive":user[9]}
       
        cur.execute(f"""SELECT * FROM RMC_Teams""")
        teams = cur.fetchall()
        teams_dict = {}        
        for team in teams:
            teams_dict[team[0]] = {"team_id":team[0]}
            
            
        return {"users":user_dict, "teams":teams_dict}
    
    
    
        
        
    # ------------------------------------------------------------------------
    # Interface class is responsible for 
    # -Handling user sessions to the database array
    # -Determining user access to database sections
    # -Determining which database a user is accessing
    # -Locking and accessing the datbase in question
    # 
    # There should be ONE db call for each db file
    #   -reading
    #   -updating
    #   -writing
    # With their own own permissions. 
    # ------------------------------------------------------------------------
    
class SharepointDatabaseInterface:
    def __init__(self, env, source, user,):
        self.env = env
        
        self.login_data = localserver_login(source)
        self.users = self.login_data["users"]
        self.teams = self.login_data["teams"]
        # self.user = self.users[getpass.getuser().upper()]
        self.user = self.users["R908394"]
        # self.permission = self.users[getpass.getuser().upper()]["privilige"]
        self.permission = self.users["R908394"]["privilige"]
        
        self.session_data = {}
        
        # database_source should follow the format bellow
        self.db_dictionary = {}
        self.database_source = {"source":str, "name":str, "env":str}
        self.active_connection = None
        
        
        # TODO the locking functions should be imported from the external
        # .py file YET TO BE MADE.
        # These should take the database function source and test for 
        # lock. 
        
        
    @restrict_function_permission(1)
    def testing_permissions(self):
        print(self.permission)
        
        
    # wrap this with a decorator?
    @restrict_function_permission(2)
    def set_db_source(self, new_db_source):
        self.database_source['source'] = new_db_source['source']
        self.database_source['name'] = new_db_source['name']
        self.database_source['env'] = new_db_source['env']
        return 1
    
    @restrict_function_permission(2)
    def set_source_request_lock(self, desired_db_source, lock_level=0):        
        self.set_db_source(desired_db_source)
        self.active_connection = dbo.request_db_lock(self, lock_level)
        return self.active_connection
    
    @restrict_function_permission(2)
    def relinquish_lock(self):
        dbo.relinquish_db_lock(self)
        print(self.active_connection)
        return 1
        
        
    @restrict_function_permission(0)
    def test_db_operations(self, dbsrc):
        with self.set_source_request_lock(dbsrc["db_source"], dbsrc["lock_level"]) as conn:
            dbo.test_dbo(conn, None)
            print(conn)
        return 1
    
