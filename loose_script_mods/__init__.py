# from. import something



__doc__ = """
pyrail is a Python library designed for easily working with Queensland Rail
specific business data. Its functionality can be broadly divided into 3 components:
importing, checking, and exporting business data. It works primarily with
train schedule and crew roster data, with some limited geography knowledge.
Some business rules have been incorporated into reference data - in particular,
rules regarding the structure of train crew job cards.

Features:
---------
General:
- Easy importing of data from primary sources (Sapiens for roster, ViziRail for
  schedule)
- Comprehensive merging of roster and schedule information - joining the
  systems where no native relationship exists
- Repository for data-based (instead of document-based) storage of business
  rules and reference data
- Easy base to expand to new functionalities when new reports/checks are
  required

Trains:
- Functionality to export schedule/individual trains in multiple different
  formats, to meet various business needs
  * Train Notices
  * HASTUS
  * Native ViziRail format (faster, various bugs patched)
  * Broadsheets
  * JSON
- Restructures ViziRail data into a convenient matrix format for easier
  manipulation and exporting
- Verification of timetable data to detect various possible planning and system
  errors
- Merging of unit data from train number to fill gaps in ViziRail data

Job Cards:
- Comprehensive checking of roster data against train schedule and roster
  protocols
- Roster data JSON export (can include details from check)
- Filling in gaps in train crew schedule information by joining with ViziRail
  data

Geography:
- Consolidation of different geography reference sources to enable merging
  different data sources, as well as exporting from one format to another
  * ViziRail
  * Sapiens
  * HASTUS
  * UTC Zones
  * pyrail (for customised Train Notice export formatting)

Often the reference data is designed to fill gaps and patch errors in existing
data. Where possible, definitive external sources should be used to minimise
internal data maintenance. However, bear in mind that bad data is endemic in
QR.
"""
