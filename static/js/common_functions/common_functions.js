function set_date(days_ahead){
    if (days_ahead != null){
        var new_date = new Date();
        new_date.setHours(+10);
        new_date.setDate(new_date.getDate() + days_ahead);
    }

    return(new_date)
}


function data_print_test(data){
    dtype = typeof(data)
    $("#debug_dom_element").attr("transient_data", `emptied`)
    console.log(`data_print_test: {data}`)
    $("#debug_dom_element").attr("transient_data", `${dtype}`)
    return(true)
}

// example ajax call format
// These are the MINIMUM required variables for any AJAX call to function
// - su_code = planner id
// - priority 0 = read, 1 = write, 2 = BIG write
// - sqldb = database location as defined in the server settings
function template_for_calling_mods_functions_with_db(){
    args = [0, 1, 2]
    kwargs = {su_code:"R908394", priority:1, sqldb:"prototype_redbook_s3_PROD"}
    request_server_route(args, kwargs, `call_mod,${'redbook_unbound'},${'get_local_bids'}`, redbook_display_work_items)
}



function transform_date_into_readable_naffing_format(date) {


}

function ms_to_twennyfour(ms) {
    if (ms == null) {
        return ''
    }

    var result = new Date(ms)
    result.setHours(result.getHours() - 10);
        hours = result.getHours()
        if (hours < 10) {
            hours = "0" + String(hours)
        } else {
            hours = String(hours)
        }
        minutes = result.getMinutes()
        if (minutes < 10) {
            minutes = "0" + String(minutes)
        } else {
            minutes = String(minutes)
        }
    result = hours + minutes

    return result
}

function html_nulled(value){
    if (value == null) {return ''}
    else {return value}
}
function flag_bad_error(value) {
    if (value != null || value == true) {return "urgent_error_background"}
    else {return ''}
}
