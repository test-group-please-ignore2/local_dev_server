function aj_test_db_operations(args) {
    $.ajax({
        url: `/test_db_operations/`,
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({args}),
        dataType: "json",
        type: 'POST',
        success: function(data){
        },
        error: function(error){
            console.log(error);
        }
    });
}

function request_server_route(kwargs, sqldb, priority, url_to_call, post_call_function, function_arguments) {
    //args must be a dictionary, with optional nested dictionary for more
    //complicated function calls.
    console.log(url_to_call)
    $.ajax({
        url: `/${url_to_call}/`,
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({kwargs, sqldb, priority}),
        dataType: "json",
        type: 'POST',
        success: function(data){
            console.log(data)

            output_flag("Good call :)", true)
            if (post_call_function != null) {
                post_call_function(data['data'], function_arguments)
            }
        },
        error: function(error){
            output_flag(error['responseText'], false)
            console.log(error);
        }
    });
}


function output_flag(response_text, flag){
    flag_html = ``
    if (flag == false) {
        flag_html += `<div type="button" id="" class="btn_alert btn_alert_bad">${response_text}</div>`
    } else {flag_html += `<div type="button" id="" class="btn_alert btn_alert_good">${response_text}</div>`}

    $('#session_alerts').prepend(flag_html)
}
