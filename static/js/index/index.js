function server_login(f_call) {
    let function_req = {login:"itsamemario"}
    $.ajax({
        url: '/login/',
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({function_req}),
        dataType: "json",
        type: 'POST',
        success: function(data){
            f_call(data)
        },
        error: function(error){
            console.log(error);
        }
    });
}

function reset_containers() {
    let main_window = document.getElementById("main_window_content")
    let main_window_toolbar = document.getElementById("main_window_toolbar_form")
    let filters = document.getElementById("session_common_filters_content")

    while (main_window.firstChild) {main_window.removeChild(main_window.lastChild)}
    while (main_window_toolbar.firstChild) {main_window_toolbar.removeChild(main_window_toolbar.lastChild)}
    while (filters.firstChild) {filters.removeChild(filters.lastChild)}

    $("#main_window_content").empty()
    $("#main_window_toolbar_form").empty()
    $("#session_common_filters_content").empty()
}


$(document).ready(function(){
    var active_data = server_login(set_active_data)

    function set_active_data(data) {
        active_data = data
        console.log(active_data)
    }

    $(".nav_button").click(function(){
        $(".btn_nav_focus").removeClass("btn_nav_focus")
        $(this).addClass("btn_nav_focus")
    })

    $("#test").click(function(){get_timetable("2021-04-15")})



    $("#activate_universe_sim").click(function(){
        reset_containers()
        activate_universe_sim()
        toggle_session_filters(show=false)
        toggle_main_form_container(show=true)
        console.log("activating uni sim context");
    })



    $("#home").click(function(){
        reset_containers()
        activate_greenscreen_context()
        $(".btn_nav_focus").removeClass("btn_nav_focus")
        toggle_session_filters(show=false)
        toggle_main_form_container(show=false)
    })



}) // end dcument ready
