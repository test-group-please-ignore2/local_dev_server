
function set_base_container_sizes() {
    desired_width = window.innerWidth - 250
    desired_height = window.innerHeight - 75

    $('#main_window').css("width", desired_width);
    $('#main_window').css("height", desired_height);
    $('#navigation_links').css("width", desired_width);

    console.log(desired_width);
}

function toggle_main_form_container(show) {
    if (show != undefined && show == true) {
        $("#main_window_toolbar").css("width", "50%")
        $("#main_window_toolbar").css("height", "90%")
        $("#main_window_toolbar").data("active", true)
        return
    } else if (show == false) {
        $("#main_window_toolbar").css("width", "45px")
        $("#main_window_toolbar").css("height", "45px")
        $("#main_window_toolbar").data("active", false)
        return
    }

    if ($("#main_window_toolbar").data("active") == false){
        $("#main_window_toolbar").css("width", "50%")
        $("#main_window_toolbar").css("height", "90%")
        $("#main_window_toolbar").data("active", true)

    } else if($("#main_window_toolbar").data("active") == true){
        $("#main_window_toolbar").css("width", "45px")
        $("#main_window_toolbar").css("height", "45px")
        $("#main_window_toolbar").data("active", false)
    }
    $("#main_window_toolbar")
}
function toggle_session_filters (show) {
    if (show != undefined && show == true) {
        $("#session_common_filters").css("width", "inherit")
        $("#session_common_filters").css("right", "-240px")
        $("#session_common_filters").data("active", true)
        return
    } else if (show == false) {
        $("#session_common_filters").css("width", "0px")
        $("#session_common_filters").css("right", "-15px")
        $("#session_common_filters").data("active", false)
        return
    }

    if ($("#session_common_filters").data("active") == false){
        $("#session_common_filters").css("width", "inherit")
        $("#session_common_filters").css("right", "-240px")
        $("#session_common_filters").data("active", true)

    } else if($("#session_common_filters").data("active") == true){
        $("#session_common_filters").css("width", "0px")
        $("#session_common_filters").css("right", "-15px")
        $("#session_common_filters").data("active", false)
    }
}


$(document).ready(function(){
    set_base_container_sizes()

    $("#toggle_main_window_form_bar").click(function(){
        toggle_main_form_container()
    })
    $("#session_filters").click(function(){
        toggle_session_filters()
    })
    $("#session_common_filters_title").click(function(){
        toggle_session_filters()
    })



    $("#main_window_toolbar").hover(function(){
        $("#toggle_main_window_form_bar").css("outline", "2px solid black");
        }, function(){
        $("#toggle_main_window_form_bar").css("outline", "1px solid black");
    })

})

$(window).resize(function(){
    set_base_container_sizes()
});
