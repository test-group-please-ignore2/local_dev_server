# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 13:48:08 2021

QR Portable Server Launcher

Build this file using pyinstaller and create a shortcut to it.
Then link the shortcut to the QR PyApp launchers cfg file to enable 
remote launching.


@author: r908394
"""

from flask import Flask, redirect, url_for, request, render_template
import sqlite3
import subprocess
import sys
import os
import getpass
import pandas as pd
import numpy as np
import datetime as dt
import time
import ast
import webbrowser

import holidays
import random
import openpyxl
import pyodbc
import networkx

import importlib as importer
import json

from docxtpl import DocxTemplate
import jinja2
import docx

from loose_script_mods.server_scripts.resource_path import resource_path

import threading
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import uic

cwd = os.getcwd()
# path = "nothing special here yet, use a system path for full effect :p"
# os.chdir(path)
print(cwd)
print("cwd --------------------------------- cwd")
ui_MainWindow, QtBaseClass = uic.loadUiType("ui\\main.ui")

class Worker(QRunnable):
    def __init__(self, *args, **kwargs):
        super(Worker, self).__init__()
        self.args = args
        self.kwargs = kwargs
        self.flask_app = None

    # Worker thread,
    # place code that needs multithreading in classes like this one
    # imports application.py as flask_app to be run in a worker thread
    # worker threads are closed on gui exit, currently not programmed to
    # close according to any other signals.
    @pyqtSlot()
    def run(self):
        self.flask_app = importer.import_module("portable_server")
        print(self.args[0])
        print(self.flask_app)
        print("Thread start")
        self.flask_app.ui = self.args[0]
        self.flask_app.application.run('0.0.0.0',9051)
        print("Thread complete")


class MainApp(QMainWindow):
    def __init__(self):
        super(MainApp, self).__init__()
        self.ui = ui_MainWindow()
        self.ui.setupUi(self)
        self.threadpool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())
        self.worker = None

        #Perform any critical functions before the program closes
        quit = QAction("Quit", self)
        quit.triggered.connect(self.closeEvent)



    def start_server(self):
        self.worker = Worker(self)
        self.threadpool.start(self.worker)
        self.ui.main_title.setText("RMC Localserver")

        url = "http://localhost:9051/"
        chrome_path = "C:\Program Files\Google\Chrome\Application\chrome"
        webbrowser.register('chrome', None, webbrowser.BackgroundBrowser(chrome_path))
        webbrowser.get('chrome').open_new_tab(url)



    def closeEvent(self, event):
        print('closing...')
        event.accept()


if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainApp()
    window.show()
    window.start_server()

    ret=app.exec_()
    sys.exit(ret)




















